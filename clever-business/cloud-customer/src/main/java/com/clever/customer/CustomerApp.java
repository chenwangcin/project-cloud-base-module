package com.clever.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ChenWang
 * @date 2020/12/12 09:10
 * @since JDK 1.8
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CustomerApp {
    public static void main(String[] args) {
        SpringApplication.run(CustomerApp.class,args);
    }
}
