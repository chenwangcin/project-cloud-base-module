package com.clever.customer.controller;

import com.clever.commons.result.BaseResult;
import com.clever.customer.request.LoginByPhoneRequestParam;
import com.clever.customer.service.CustomerService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.Pattern;

/**
 * @author ChenWang
 * @date 2020/12/12 10:39
 * @since JDK 1.8
 */
@RestController
@RequestMapping("test")
public class CustomerController {
    @Resource
    CustomerService customerService;
    /**
     * 验证码
     */
    @PostMapping("/captcha")
    public BaseResult<Boolean> captcha(
            @RequestParam @Pattern(regexp = "^1[3-9][0-9]{9}$",message = "手机格式错误")String phone){
        boolean flag = customerService.askVerificationCode(phone);
        return BaseResult.success(flag);
    }
    /**
     * 通过电话登录  简单版本  无加密，无第三方框架
     */
    @PostMapping("/login-by-phone-simple")
    public BaseResult<Boolean> simpleLoginByPhone(@RequestBody @Validated LoginByPhoneRequestParam loginByPhoneRequestParam){
        boolean flag =  customerService.simpleLoginByPhone(loginByPhoneRequestParam);
        return BaseResult.success(flag);
    }
    /**
     * 通过电话登录  集成第三方验证框架 shiro
     */
    @PostMapping("/login-by-phone")
    public BaseResult<Boolean> loginByPhone(@RequestBody @Validated LoginByPhoneRequestParam loginByPhoneRequestParam){
        boolean flag =  customerService.loginByPhone(loginByPhoneRequestParam);
        return BaseResult.success(flag);
    }
}
