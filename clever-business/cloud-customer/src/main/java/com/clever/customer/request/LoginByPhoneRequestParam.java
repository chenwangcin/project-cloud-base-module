package com.clever.customer.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author ChenWang
 * @date 2020/12/12 11:45
 * @since JDK 1.8
 */
@Data
public class LoginByPhoneRequestParam {
    /**
     * 电话
     */
    @NotNull
    @Pattern(regexp = "^1[3-9][0-9]{9}$",message = "手机格式错误")
    private String phone;
    /**
     * 手机验证码
     */
    @NotNull
    @Pattern(regexp = "^[0-9]{6}$",message = "您输入的手机验证码格式有误")
    private String captcha;
    /**
     * 校验码
     */
//    @NotNull
//    @Pattern(regexp = "^[0-9a-zA-Z]{4}$",message = "您输入的校验码格式有误")
    private String code;
}
