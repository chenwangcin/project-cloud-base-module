package com.clever.customer.service;

import com.clever.customer.request.LoginByPhoneRequestParam;

/**
 * @author ChenWang
 * @interfaceName CustomerService
 * @date 2020/12/12 10:48
 * @since JDK 1.8
 */
public interface CustomerService {
    /**
     * 异步请求向指定电话发送验证码
     * @param phone 指定的电话号码
     * @return true表示发送成功,false表示发送失败
     */
    boolean askVerificationCode(String phone);
    /**
     * 简单版本的通过电话登录 无加密，没有集成第三方框架shiro
     * @param loginByPhoneRequestParam
     * @return true表示登录成功,false表示登录失败
     */
    boolean simpleLoginByPhone(LoginByPhoneRequestParam loginByPhoneRequestParam);

    /**
     * 集成了第三方验证框架 使用电话登录 验证码加密
     * @param loginByPhoneRequestParam  登录请求的数据
     * @return  true表示登录成功,false表示登录失败
     */
    boolean loginByPhone(LoginByPhoneRequestParam loginByPhoneRequestParam);
}
