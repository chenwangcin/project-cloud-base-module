package com.clever.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.util.Properties;

/**
 * @author ChenWang
 * @date 2020/12/12 09:08
 * @since JDK 1.8
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ProducerApp {
    public static void main(String[] args) {
        SpringApplication.run(ProducerApp.class,args);
    }
}
