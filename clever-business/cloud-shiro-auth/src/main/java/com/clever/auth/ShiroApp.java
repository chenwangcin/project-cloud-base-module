package com.clever.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ChenWang
 * @date 2020/12/14 16:04
 * @since JDK 1.8
 */
@SpringBootApplication
@MapperScan("com.clever.**.mapper")
public class ShiroApp {
    public static void main(String[] args) {
        SpringApplication.run(ShiroApp.class,args);
    }
}
