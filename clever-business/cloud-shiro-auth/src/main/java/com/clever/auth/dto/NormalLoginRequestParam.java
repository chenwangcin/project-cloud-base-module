package com.clever.auth.dto;

import lombok.Data;

/**
 * @author ChenWang
 * @date 2020/12/14 00:29
 * @since JDK 1.8
 */
@Data
public class NormalLoginRequestParam {
    /**
     * 账户名
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 校验码
     */
    private String code;
}
