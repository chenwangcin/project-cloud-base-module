package com.clever.auth.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author ChenWang
 * @date 2020/12/14 20:54
 * @since JDK 1.8
 */
@Data
public class PermissionDto {
    private Integer perId;

    /**
     * 权限名称
     */
    private String perName;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 权限说明
     */
    private String perDesc;

    List<RoleDto> roles;
}
