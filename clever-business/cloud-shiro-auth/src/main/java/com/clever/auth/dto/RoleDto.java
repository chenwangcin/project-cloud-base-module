package com.clever.auth.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author ChenWang
 * @date 2020/12/14 20:53
 * @since JDK 1.8
 */
@Data
public class RoleDto {
    private Integer roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 角色说明
     */
    private String roleDesc;

    List<PermissionDto> permissions;

}
