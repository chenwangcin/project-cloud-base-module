package com.clever.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clever.auth.entity.Member;

public interface MemberMapper extends BaseMapper<Member> {
}