package com.clever.auth.mapper;

import com.clever.auth.dto.RoleDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ChenWang
 * @interfaceName MemberRoleRelationMapper
 * @date 2020/12/14 20:56
 * @since JDK 1.8
 */
public interface MemberRoleRelationMapper {
    List<RoleDto> selectByMemberId(@Param("mid") Integer mid);
}
