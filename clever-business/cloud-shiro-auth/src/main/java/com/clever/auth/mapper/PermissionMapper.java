package com.clever.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clever.auth.entity.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {
}