package com.clever.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clever.auth.entity.Role;

public interface RoleMapper extends BaseMapper<Role> {
}