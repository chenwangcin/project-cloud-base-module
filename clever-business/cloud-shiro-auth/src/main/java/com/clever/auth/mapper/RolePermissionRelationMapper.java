package com.clever.auth.mapper;

import com.clever.auth.dto.PermissionDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ChenWang
 * @interfaceName RolePermissionRelationMapper
 * @date 2020/12/14 21:06
 * @since JDK 1.8
 */
public interface RolePermissionRelationMapper {
    List<PermissionDto> selectAllByRoleId(@Param("roleId")Integer roleId);
}
