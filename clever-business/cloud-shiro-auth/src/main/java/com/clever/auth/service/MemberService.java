package com.clever.auth.service;

import com.clever.auth.dto.MemberDto;

/**
 * @author ChenWang
 * @interfaceName MemberService
 * @date 2020/12/14 17:25
 * @since JDK 1.8
 */
public interface MemberService {
    MemberDto findByUsername(String username);
}
