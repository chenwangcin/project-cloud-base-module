package com.clever.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.clever.auth.dto.MemberDto;
import com.clever.auth.entity.Member;
import com.clever.auth.mapper.MemberMapper;
import com.clever.auth.service.MemberService;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;

/**
 * @author ChenWang
 * @date 2020/12/14 17:26
 * @since JDK 1.8
 */
public class MemberServiceImpl implements MemberService {
    @Resource
    MemberMapper memberMapper;
    @Override
    public MemberDto findByUsername(String username){
        QueryWrapper<Member> qw = new QueryWrapper();
        qw.lambda().eq(Member::getUsername,username);
        Member member = memberMapper.selectOne(qw);
        MemberDto memberDto = new MemberDto();
        BeanUtils.copyProperties(member,memberDto);
        return memberDto;
    }
}
