package com.clever.auth.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;

/**
 * @author ChenWang
 * @date 2020/12/14 00:24
 * @since JDK 1.8
 */
public class ShiroUtil {

    public static final int DEFAULT_ITERATIONS = 100;

    /**
     * 密码进行加密 [注册的时候可以使用]
     */
    public static String encrypt(String password) {
        return new SimpleHash(Sha256Hash.ALGORITHM_NAME, password, null, DEFAULT_ITERATIONS).toHex();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static void logout() {
        getSubject().logout();
    }

}
