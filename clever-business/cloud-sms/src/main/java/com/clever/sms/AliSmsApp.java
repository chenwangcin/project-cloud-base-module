package com.clever.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ChenWang
 * @date 2020/12/12 15:48
 * @since JDK 1.8
 */
@SpringBootApplication(scanBasePackages = "com.clever")
public class AliSmsApp {
    public static void main(String[] args) {
        SpringApplication.run(AliSmsApp.class,args);
    }
}
