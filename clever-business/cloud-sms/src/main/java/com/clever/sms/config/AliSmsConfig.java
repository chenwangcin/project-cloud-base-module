package com.clever.sms.config;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.Resource;

/**
 * @author ChenWang
 * @date 2020/12/12 14:59
 * @since JDK 1.8
 */
@Configuration
public class AliSmsConfig {
    @Resource
    AliSmsProperties aliSmsProperties;
    @Bean
    @Primary
    public IAcsClient iAcsClient(){
        DefaultProfile profile = DefaultProfile.getProfile(
                aliSmsProperties.getRegionId(),
                aliSmsProperties.getAccessKeyId(),
                aliSmsProperties.getAccessSecret());
        return new DefaultAcsClient(profile);
    }

}
