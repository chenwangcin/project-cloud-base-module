package com.clever.sms.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ChenWang
 * @date 2020/12/12 15:00
 * @since JDK 1.8
 */
@Data
@Component
@ConfigurationProperties(prefix = "ali.sms")
public class AliSmsProperties {
    /**
     * 授权ID
     */
    private String accessKeyId;
    /**
     * 授权密钥
     */
    private String accessSecret;
    /**
     * 范围ID
     */
    private String regionId;
    /**
     * 作用域
     */
    private String domain;
    /**
     * 版本号 固定值
     */
    private String version;
    /**
     * 签名 根据创建的签名来设置
     */
    private String signName;
    /**
     * 在后台添加模板时阿里云后台生产
     */
    private String templateCode;

    private String action;

    public static final String SIGN_NAME = "SignName";
    public static final String REGION_ID = "RegionId";
    public static final String TEMPLATE_CODE = "TemplateCode";
    public static final String PHONE_NUMBERS = "PhoneNumbers";
    public static final String TEMPLATE_PARAM = "TemplateParam";
}
