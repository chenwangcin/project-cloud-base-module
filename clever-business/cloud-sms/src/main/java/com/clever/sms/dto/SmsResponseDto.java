package com.clever.sms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ChenWang
 * @date 2020/12/12 15:37
 * @since JDK 1.8
 *
 * 示例数据：
 * {"RequestId":"A7AE3000-17A7-450F-ADBA-08075F16822E",
 * "Message":"OK","BizId":"884311207344181058^0","Code":"OK"}
 */

@Data
public class SmsResponseDto implements Serializable {
    @JsonProperty("RequestId")
    private String requestId;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("BizId")
    private String bizId;
    @JsonProperty("Code")
    private String code;
    public static final String SMS_RESPONSE_CODE_OK = "OK";
}
