package com.clever.sms.listener;

import com.clever.sms.service.SimpleSmsMessageService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author ChenWang
 * @date 2020/12/12 14:08
 * @since JDK 1.8
 */
@Component
@RocketMQMessageListener(consumerGroup = "cw-test",topic = "captcha-cw")
public class MessageListener implements RocketMQListener<String> {
    @Resource
    SimpleSmsMessageService simpleSmsMessageService;
    @Override
    public void onMessage(String phone) {
        simpleSmsMessageService.sendMsgByPhone(phone);
    }
}
