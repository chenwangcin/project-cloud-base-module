package com.clever.sms.service;

/**
 * @author ChenWang
 * @interfaceName smsMessageService
 * @date 2020/12/12 14:14
 * @since JDK 1.8
 */
public interface SmsMessageService {
    void sendMsgByPhone(String phone);
}
