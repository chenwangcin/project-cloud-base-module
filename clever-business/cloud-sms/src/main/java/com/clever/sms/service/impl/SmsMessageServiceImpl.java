package com.clever.sms.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.clever.redis.config.RedisKeyPrefixProperties;
import com.clever.redis.service.RedisService;
import com.clever.sms.config.AliSmsProperties;
import com.clever.sms.dto.SmsResponseDto;
import com.clever.sms.service.SmsMessageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author ChenWang
 * @date 2020/12/12 14:42
 * @since JDK 1.8
 */
@Service
public class SmsMessageServiceImpl implements SmsMessageService {
    @Resource
    AliSmsProperties aliSmsProperties;
    @Resource
    IAcsClient iAcsClient;
    @Resource
    ObjectMapper objectMapper;
    @Resource
    RedisService redisService;
    @Resource
    RedisKeyPrefixProperties redisKeyPrefixProperties;
    @Override
    public void sendMsgByPhone(String phone) {
        String captcha = RandomUtil.randomNumbers(6);
        try{
            CommonRequest commonRequest = getCommonRequest(phone, captcha);
            // 请求阿里云根据配置信息发送短信 并且获得该请求的反馈
            CommonResponse commonResponse = iAcsClient.getCommonResponse(commonRequest);
            String data = commonResponse.getData();
            // 转换成json字符串       objectMapper.writeValueAsString()
            // 从json字符串里读数据 -> 解析反馈信息
            SmsResponseDto smsResponseDto = objectMapper.readValue(data, SmsResponseDto.class);
            // 判定
            if(Objects.nonNull(smsResponseDto)&& SmsResponseDto.SMS_RESPONSE_CODE_OK.equals(smsResponseDto.getCode())){
                //将验证码存入到redis中,有效时间为五分钟
                redisService.setEx(String.format("%s:%s",redisKeyPrefixProperties.getSmsPrefix(),phone),
                        captcha,5, TimeUnit.MINUTES);
            }
        } catch (ClientException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    /**
     * 参考官方文档发现代码太多了，进行部分封装
     * @param phone     电话
     * @param captcha   电话验证码
     * @return          指定的请求
     */
    private CommonRequest getCommonRequest(String phone,String captcha){
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(aliSmsProperties.getDomain());
        request.setSysVersion(aliSmsProperties.getVersion());
        request.setSysAction(aliSmsProperties.getAction());
        request.putQueryParameter(AliSmsProperties.REGION_ID, aliSmsProperties.getRegionId());
        request.putQueryParameter(AliSmsProperties.PHONE_NUMBERS, phone);
        request.putQueryParameter(AliSmsProperties.SIGN_NAME, aliSmsProperties.getSignName());
        request.putQueryParameter(AliSmsProperties.TEMPLATE_CODE, aliSmsProperties.getTemplateCode());
        /*request.putQueryParameter(AliSmsProperties.TEMPLATE_PARAM, "{\"code\":\"" + captcha + "\"}");*/
        request.putQueryParameter(AliSmsProperties.TEMPLATE_PARAM, String.format("{\"code\":\"%s\"}",captcha));
        return request;
    }
}
