package com.clever.commons.exception;

/**
 * @author ChenWang
 * @date 2020/12/11 19:57
 * @since JDK 1.8
 */
public class MySqlException extends BaseException {
    public MySqlException(String message, int status, String msg, String tip) {
        super(message, status, msg, tip);
    }

    public MySqlException(StatusCode statusCode) {
        super(statusCode);
    }
}
