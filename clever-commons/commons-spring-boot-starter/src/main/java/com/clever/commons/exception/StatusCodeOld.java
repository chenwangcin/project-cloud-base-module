package com.clever.commons.exception;

/**
 * @author ChenWang
 * @date 2020/12/11 19:39
 * @since JDK 1.8
 */
public enum StatusCodeOld {

    /**
     * 成功
     */
    SUCCESS(2000, "sucess", "成功"),
    /**
     * 错误
     */
    ERROR(404, "error", "失败"),
    /**
     * 错误
     */
    SERVER_ERROR(500, "server error", "服务器出错"),


    /**
     * 数据库更新用户数据错误
     */
    DATABASE_UPDATE_USER_ERROR(60010, "database update user data failed", "数据库更新用户数据失败"),
    /**
     * 数据库更新用户数据错误
     */
    DATABASE_UPDATE_PRICE_ERROR(60011, "database update price data failed", "数据库更新价格数据失败"),
    /**
     * 用户数据格式错误
     */
    DATABASE_CHECK_STATUS_ERROR(60011, "database check status out of range", "审核状态异常"),

    /**
     * 用户头像上传错误
     */
    SERVER_UPLOAD_ERROR(50010, "server upload file error", "服务器上传文件出错"),


    /**
     * 业务层错误
     */
    SERVICE_ERROR(50040, "service error", "业务层出错"),

    /**寄卖单异常*/
    /**
     * 寄卖单不存在
     */
    CONSIGNMENT_NOT_EXITS(20001, "consignment is not exist ", "寄卖单不存在"),
    /**
     * 寄卖单已删除
     */
    CONSIGNMENT_HASE_BEEN_DELETE(20002, "consignment has been delete", "寄卖单已经删除"),
    /**
     * 寄卖单商品信息读取异常
     */
    CONSIGNMENT_DATA_READ_ERROR(20003, "consignment data read erroe", "寄卖单商品信息读取异常"),


    /**
     * 用户数据不存在
     */
    USER_NOT_SUFFICIENT_FUNDS(40002, "user doesn't posess sufficient funds ", "用户余额不足"),
    /**
     * 用户数据格式错误
     */
    USER_ERROR(40001, "user fomat error", "用户数据格式错误"),
    /**
     * 用户数据不存在
     */
    USER_NOT_EXIST_ERROR(40002, "user is not exist ", "用户数据不存在"),

    /**
     * 数据格式有误
     */
    DATA_FORMAT_ERROR(40003, "Incorrect data format", "数据格式有误"),
    /**
     * 数据不存在
     */
    NO_DATA_ERROR(40004, "no data", "数据不存在"),
    /**
     * 数据为空
     */
    NULL_ERROR(40005, "Data is empty", "数据为空"),
    /**
     * 游客没有权限
     */
    USER_TOURIEST_ERROR(40006, "Incorrect touriest visit", "游客访问权限错误"),
    /**
     * 数据库录入失败
     */
    ADD_DATABASE_DATA_ERROR(40007, "Database entry failure", "数据库录入失败"),
    /**
     * 数据修改失败
     */
    UPDATE_DATA_ERROR(40008, "Data modification failed", "数据修改失败"),
    /**
     * 数据删除失败
     */
    DEL_DATA_ERROR(40009, "Data deletion failed", "数据删除失败"),
    /**
     * 用户微信账号信息错误
     */
    USER_VECHAT_ERROR(40090, "Incorrect vechat account", "不正确的微信账号"),

    /**
     * 寄送单数据不存在
     */
    USER_CONSIGMENT_NOT_EXIST_ERROR(400010, "user is not exist ", "用户数据不存在"),


    /**
     * 产品的异常
     */
    PRODUCT_NOT_EXIST_ERROR(30001, "product store data is not exist", "产品库存数据不存在"),

    /**
     * 订单错误
     */
    ORDER_IS_NO(40060, "Order is empty", "用户订单不存在"),
    ORDER_NULL(40061, "The added order data is empty", "订单数据为空"),
    ORDER_INSERT_NULL(40065, "The added data is empty", "提交的数据为空"),
    ORDER_INSERT_ERROR(40066, "Add failure", "订单添加失败"),
    ORDER_DELETE_ERROR(40068, "delete failure", "删除失败"),
    ORDER_REFUND(40070, "The order is subject to refund", "该订单在退款中"),
    ORDER_UNPAID(40071, "Order unpaid", "订单未付款"),
    ORDER_PAY_ERROR(40072, "payment failure fail to pay", "付款失败"),
    ORDER_PAY_FOR_ERROR(40073, "This order has been paid and cannot be repaid", "该订单已付过款，不可重复付款"),
    ORDER_STATUS_ERROR(40075, "The state of the submission is incorrect", "提交的状态值有误"),
    ORDER_UNFINISHED_ERROR(40077, "Order not completed", "订单未完成"),

    SHOPPINGCART_OPERATION_ERROR(40081, "Shopping cart operation failed", "购物车操作失败"),
    BALANCE_ERROR(40083, "Insufficient account balance", "账户余额不足"),
    TRADING_ERROR(40084, "Transaction failure", "交易失败"),
    PASSWORD_ERROR(40085, "wrong password", "密码错误"),
    USER_ID_ERROR(40086, "User data is incorrect", "用户数据有误"),
    MESSAGE_ADD_ERROR(40087, "Message addition failed", "消息添加失败"),
    SELLER_DATA_ERROR(40088, "Incorrect seller information", "卖家信息有误"),
    TRANSACTION_RECORDS_ERROR(40089, "Transaction record addition failed", "交易记录添加失败"),

    REFUND_FOR_ERROR(40021, "Do not reapply for a refund", "不可重复申请退款"),
    REFUND_DATA_ERROR(40023, "Refund data does not exist", "退款数据不存在"),

    /**
     * 数据验证错误
     */
    PRICE_ERROR(40071, "The price is wrong, please make a new order", "价格有误，请重新下单"),
    STOCKPILE_ERROR(40073, "Insufficient inventory", "库存不足"),
    PRODUCT_NOT_ERROR(40073, "The merchandise is off the shelves", "商品已下架"),
    STATUS_UPDATE_ERROR(40075, "The state value to be modified must not be less than the state value in the database", "要修改的状态值，不可小于数据库中的状态值"),
    ;

    /**
     * 状态码
     */
    private int status;
    /**
     * 英文信息提示
     */
    private String msg;
    /**
     * 中文信息提示
     */
    private String tip;

    StatusCodeOld(int status, String msg, String tip) {
        this.status = status;
        this.msg = msg;
        this.tip = tip;
    }

    private void setStatus(int status) {
        this.status = status;
    }

    private void setMsg(String msg) {
        this.msg = msg;
    }

    private void setTip(String tip) {
        this.tip = tip;
    }
}

