package com.clever.commons.utils;

/**
 * @author ChenWang
 * @interfaceName ColaBeanUtilsCallBack
 * @date 2020/12/11 20:04
 * @since JDK 1.8
 */
@FunctionalInterface
public interface ColaBeanUtilsCallBack<S,T> {
    void callBack(S t, T s);
}
