package com.clever.redislock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ChenWang
 * @date 2020/12/11 19:06
 * @since JDK 1.8
 */
@SpringBootApplication
public class LockApp {
    public static void main(String[] args) {
        SpringApplication.run(LockApp.class,args);
    }
}
