package com.clever.redis.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

/**
 * @author ChenWang
 * @date 2020/12/11 17:34
 * @since JDK 1.8
 */
@Configuration
public class RedisConfig {
    /*@Resource 官方提供的，我们要定制其成为String,Object类型-->然后再封装成五种redis类型的对应方法
    RedisTemplate<Object,Object> redisTemplate;*/
    /*@Resource 官方提供的, 基本不用
    StringRedisTemplate<String,String> stringStringStringRedisTemplate;*/

    /**
     * 直接存对象会抛出异常，需要自定义序列化  不序列化的话会存二进制数据
     * java自带的普通序列化可以传对象到value中，但是是二进制码
     * 可以通过jackson或者fastjason进行序列化-->提高可读性
     * @param factory
     * @param redisSerializer
     * @return
     */
    @Bean
    @Primary
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory factory, RedisSerializer<Object> redisSerializer){
        RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();
        // 开启事务支持(可选)
        redisTemplate.setEnableTransactionSupport(true);
        //默认的String类型的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        //key是String,直接key的序列化
        redisTemplate.setKeySerializer(stringRedisSerializer);
        //hash key值也是直接String序列化,
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        //普通value通过jackson序列化
        redisTemplate.setValueSerializer(redisSerializer);
        //hash value通过jackson序列化
        redisTemplate.setHashValueSerializer(redisSerializer);
        //将自定义的redisTemplate注册到工厂中
        redisTemplate.setConnectionFactory(factory);
        return redisTemplate;
    }

    /**
     * 设置redis值的默认的序列化的方式为jackson
     * 还可以设置为fast jason，那个更加简单
     * @return 序列化类
     */
    @Bean
    @Primary
    public Jackson2JsonRedisSerializer<Object> redisSerializer(){
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        return jackson2JsonRedisSerializer;
    }
    

    /**
     * String类型的处理类
     */
    @Bean
    public ValueOperations<String, Object> valueOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForValue();
    }
    /**
     * hash类型的处理类
     */
    @Bean
    public HashOperations<String, String, Object> hashOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForHash();
    }
    /**
     * 普通对象集合类型的处理类
     */
    @Bean
    public ListOperations<String, Object> listOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForList();
    }
    /**
     * 无序set类型的处理类
     */
    @Bean
    public SetOperations<String, Object> setOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForSet();
    }
    /**
     * 有序set类型的处理类
     */
    @Bean
    public ZSetOperations<String, Object> zSetOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForZSet();
    }
}
