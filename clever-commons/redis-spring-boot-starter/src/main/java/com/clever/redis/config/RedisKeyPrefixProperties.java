package com.clever.redis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ChenWang
 * @date 2020/12/12 15:53
 * @since JDK 1.8
 */
@Component
@ConfigurationProperties(prefix = "key-prefixes")
@Data
public class RedisKeyPrefixProperties {
    /**
     * 电话验证码的前缀
     */
    private String smsPrefix;
}
