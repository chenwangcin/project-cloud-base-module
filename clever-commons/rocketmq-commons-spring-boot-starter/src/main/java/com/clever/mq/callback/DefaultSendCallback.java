package com.clever.mq.callback;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;

/**
 * @author ChenWang
 * @date 2020/12/12 11:38
 * @since JDK 1.8
 */
@Slf4j
public class DefaultSendCallback implements SendCallback {
    @Override
    public void onSuccess(SendResult sendResult) {
        log.info("success");
    }

    @Override
    public void onException(Throwable throwable) {
        log.info("failure");
        log.info(throwable.getClass().toString()+" ----> "+throwable.getMessage());
        throw new RuntimeException("消息发送失败");
    }
}
