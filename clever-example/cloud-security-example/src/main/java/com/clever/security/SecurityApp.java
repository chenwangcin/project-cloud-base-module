package com.clever.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ChenWang
 * @date 2020/12/14 10:36
 * @since JDK 1.8
 */
@SpringBootApplication
@MapperScan("com.clever.**.mapper")
public class SecurityApp {
    public static void main(String[] args) {
        SpringApplication.run(SecurityApp.class,args);
    }
}
