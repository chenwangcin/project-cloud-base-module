package com.clever.security.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ChenWang
 * @date 2020/12/14 14:56
 * @since JDK 1.8
 */
@RestController
public class WebSecurityController {
    @PostMapping("/ll")
    public String login(){
        return "ll";
    }
}
