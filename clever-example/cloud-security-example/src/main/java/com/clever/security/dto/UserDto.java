package com.clever.security.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author ChenWang
 * @date 2020/12/14 11:42
 * @since JDK 1.8
 */
@Data
public class UserDto implements UserDetails {
    /**
     * 用户Id
     */
    private Integer uid;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    @JsonIgnore
    private String password;

    /**
     * 账号是否被锁定 1 表示未锁定 0 表示锁定
     */
    private Boolean locked;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    /**
     * 账号是否过期
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账号是否被锁定
     * 是否邮件激活
     * 是否违规
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return this.locked;
    }

    /**
     * 凭证是否过期
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 用户是否删除
     * @return
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
