package com.clever.security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "sys_user")
public class SysUser {
    /**
     * 用户Id
     */
    @TableId(value = "uid", type = IdType.INPUT)
    private Integer uid;

    /**
     * 用户名
     */
    @TableField(value = "username")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "`password`")
    private String password;

    /**
     * 账号是否被锁定 1 表示未锁定 0 表示锁定
     */
    @TableField(value = "locked")
    private Boolean locked;

    public static final String COL_UID = "uid";

    public static final String COL_USERNAME = "username";

    public static final String COL_PASSWORD = "password";

    public static final String COL_LOCKED = "locked";
}