package com.clever.security.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ChenWang
 * @date 2020/12/14 15:02
 * @since JDK 1.8
 */
@Component
@Slf4j
public class LoginErrorHander implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.info("-------------------------->\n"+
                exception.getClass().toString()+"\n"+
                exception.getMessage()+"\n"+
                exception.getCause().getMessage());
    }
}
