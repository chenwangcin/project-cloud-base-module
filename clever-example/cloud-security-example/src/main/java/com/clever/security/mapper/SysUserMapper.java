package com.clever.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clever.security.entity.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}