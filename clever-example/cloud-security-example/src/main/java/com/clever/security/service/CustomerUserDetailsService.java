package com.clever.security.service;

import com.clever.security.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author ChenWang
 * @date 2020/12/14 11:40
 * @since JDK 1.8
 */
@Component
public class CustomerUserDetailsService implements UserDetailsService {
    @Resource
    UserService userService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto userDto = userService.findByUsername(username);
        if(Objects.isNull(userDto)){
            throw new RuntimeException("用户不存在");
        }
        return userDto;
    }
}
