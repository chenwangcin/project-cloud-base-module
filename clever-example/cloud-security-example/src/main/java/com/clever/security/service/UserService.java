package com.clever.security.service;

import com.clever.security.dto.UserDto;

/**
 * @author ChenWang
 * @date 2020/12/14 11:48
 * @since JDK 1.8
 */
public interface UserService {
    UserDto findByUsername(String username);
}
