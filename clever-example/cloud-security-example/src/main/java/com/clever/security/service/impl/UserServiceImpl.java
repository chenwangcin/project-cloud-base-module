package com.clever.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.clever.security.dto.UserDto;
import com.clever.security.entity.SysUser;
import com.clever.security.mapper.SysUserMapper;
import com.clever.security.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author ChenWang
 * @date 2020/12/14 11:49
 * @since JDK 1.8
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    SysUserMapper sysUserMapper;
    @Override
    public UserDto findByUsername(String username) {
        QueryWrapper<SysUser> qw = new QueryWrapper();
        qw.lambda().eq(SysUser::getUsername,username);
        SysUser sysUser = sysUserMapper.selectOne(qw);
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(sysUser,userDto);
        return userDto;
    }
}
