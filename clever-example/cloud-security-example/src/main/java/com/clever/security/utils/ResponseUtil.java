package com.clever.security.utils;

import com.clever.commons.result.BaseResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ChenWang
 * @date 2020/12/14 14:34
 * @since JDK 1.8
 */
public class ResponseUtil {
    public static void responseTpJson(
            HttpServletResponse response,
            ObjectMapper objectMapper,
            BaseResult<Object> baseResult)
            throws IOException {
        //设置响应的编码格式
        response.setCharacterEncoding("UTF-8");
        //告知请求方响应返回的数据类型是什么
        response.setContentType("application/json; charset=UTF-8");
        response.getWriter().println(objectMapper.writeValueAsString(baseResult));
    }

}
